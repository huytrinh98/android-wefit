package com.example.mywefit.ui.home;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mywefit.R;

import java.util.List;

public class HomeModel extends RecyclerView.Adapter {
    private HomeFragment activity;
    private List<ViewModel> listViewModel;
    private IOnClickItem iOnClickItemListener;

    private MutableLiveData<String> mText;

    public HomeModel(HomeFragment activity, List<ViewModel> listViewModel, HomeFragment iOnClickItemListener){
        this.activity = activity;
        this.listViewModel = listViewModel;
        this.iOnClickItemListener = iOnClickItemListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View v = inflater.inflate(R.layout.item_home,parent,false);
        ProductHolder holder = new ProductHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        ProductHolder vh = (ProductHolder) holder;
        ViewModel viewModel = listViewModel.get(position);
        vh.tvTitle.setText(viewModel.getTitle());
        vh.tvDes.setText(viewModel.getDes());
        vh.ivCover.setImageResource(viewModel.getImage());

        vh.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iOnClickItemListener.onClickItem(position);

            }
        });

    }

    @Override
    public int getItemCount() {
        return listViewModel.size();
    }
    public interface IOnClickItem {
        void onClickItem(int position);
    }

    public LiveData<String> getText() {
        return mText;
    }
    public static class ProductHolder extends RecyclerView.ViewHolder{
        private ImageView ivCover;
        private TextView tvTitle;
        private TextView tvDes;

        public ProductHolder(@NonNull View itemView) {
            super(itemView);
            ivCover = (ImageView) itemView.findViewById(R.id.ivCover);
            tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
            tvDes = (TextView) itemView.findViewById(R.id.tvDes);
        }



    }

}