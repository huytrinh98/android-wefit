package com.example.mywefit.ui.home;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.widget.Toast;

import com.example.mywefit.R;

import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends Fragment implements HomeModel.IOnClickItem {
    List<ViewModel> listViewModel = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_home, container, false);
        initData();
        HomeModel adapter = new HomeModel(this,listViewModel,this);
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(),1);
        RecyclerView rvHome = v .findViewById(R.id.rvHome);
        rvHome.setLayoutManager(layoutManager);
        rvHome.setAdapter(adapter);
        return v;
    }

    private void setContentView(int fragment_home) {
    }

    private void initData() {
        listViewModel.add(new ViewModel("4M ARMS", "Beginner . 4 Mins . 18 Calories","" ,R.drawable.arms));
        listViewModel.add(new ViewModel("5M LEGS", "Beginner . 5 Mins . 25 Calories", "",R.drawable.legs));
        listViewModel.add(new ViewModel("7 MINUTE BUTT", "Beginner . 7 Mins . 39 Calories", "",R.drawable.minutebutt));
        listViewModel.add(new ViewModel("7M ABS", "Beginner . 7 Mins . 42 Calories", "",R.drawable.abs));
        listViewModel.add(new ViewModel("7M ARMS", "Beginner . 7 Mins . 27 Calories", "",R.drawable.marms));
        listViewModel.add(new ViewModel("7M BEGINNER", "Beginner . 7 Mins . 28 Calories", "",R.drawable.beginner));
        listViewModel.add(new ViewModel("7M BUTT", "Beginner . 7 Mins . 31 Calories", "",R.drawable.butt));
        listViewModel.add(new ViewModel("7M CLASSIC", "Beginner . 7 Mins . 41 Calories", "",R.drawable.classic));
        listViewModel.add(new ViewModel("7M COMPLETE", "Beginner . 7 Mins . 27 Calories", "",R.drawable.complete));
        listViewModel.add(new ViewModel("7M CORE", "Beginner . 7 Mins . 28 Calories", "",R.drawable.core));
        listViewModel.add(new ViewModel("7M LEGS", "Beginner . 7 Mins . 33 Calories", "",R.drawable.legs));
        listViewModel.add(new ViewModel("7M NO JUMPING", "Beginner . 7 Mins . 32 Calories", "",R.drawable.jumping));
        listViewModel.add(new ViewModel("7M SWEAT", "Beginner . 7 Mins . 54 Calories", "",R.drawable.sweat));
        listViewModel.add(new ViewModel("7M THIGHS", "Beginner . 7 Mins . 31 Calories", "",R.drawable.thighs));
        listViewModel.add(new ViewModel("7M WAIST", "Beginner . 7 Mins . 34 Calories", "",R.drawable.waist));
        listViewModel.add(new ViewModel("7M WEIGHT LOSS", "Beginner . 7 Mins . 36 Calories", "",R.drawable.weightloss));
        listViewModel.add(new ViewModel("7M WOMEN", "Beginner . 7 Mins . 46 Calories", "",R.drawable.women));
        listViewModel.add(new ViewModel("10M WEIGHT LOST", "Beginner . 10 Mins . 45 Calories", "",R.drawable.weightloss));
        listViewModel.add(new ViewModel("10M BUTT", "Beginner . 10 Mins . 42 Calories", "",R.drawable.mbutt));
        listViewModel.add(new ViewModel("10M LEGS", "Beginner . 10 Mins . 34 Calories", "",R.drawable.legs));



    }

    @Override
    public void onClickItem(int position) {
        ViewModel viewModel = listViewModel.get(position);
        Toast.makeText(getActivity(), viewModel.getTitle(), Toast.LENGTH_SHORT).show();

    }
}