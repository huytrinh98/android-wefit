package com.example.mywefit.ui.home;

public class ViewModel {
    private String title;
    private String des;
    private int img;

    public ViewModel(String title, String des, String price, int img) {
        this.title = title;
        this.des = des;
        this.img = img;
    }

    public String getTitle() { return title; }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public int getImage() {
        return img;
    }

    public void setImage(int image) {
        this.img = img;
    }


}
